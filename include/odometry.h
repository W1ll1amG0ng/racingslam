#pragma once

#include "racingslam/CarTrack.h"
#include "handle.h"

#include "fsd_common_msgs/CarState.h"

// tf2 headers
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>

// pcl
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/filter.h>

// ROS common header
#include <geometry_msgs/PointStamped.h>

// ROS visualization header
#include <visualization_msgs/Marker.h>

// Eigen
#include <Eigen/Eigen>

#include <vector>
#include <memory>

namespace racingslam {

    class Odometry : public Handle {

        public:

            Odometry(std::string ns);
            virtual void loadParam() = 0;
            virtual void regTopics() = 0;
            virtual void init();
            void sendTransform();
            void sendCarTrackVisualization();

        protected:

            racingslam::CarTrack odom_track_;
            fsd_common_msgs::CarState zero_pos_;

            // flags
            bool is_inited_ = false;

            // frame id
            std::string odom_frame_name_;
            std::string world_frame_name_;
            std::string car_frame_name_;
            std::string lidar_frame_name_;

            // tf
            geometry_msgs::TransformStamped transform_lidar2car_;

            // pub & sub
            ros::Publisher odom_track_visualization_pub_;
            std::string odom_track_visualization_topic_;

            ros::Publisher odom_track_pub_;
            std::string odom_track_topic_;

            // Only could be used by derived class
            void loadBasicParam();
            void regBasicTopics() {
                odom_track_visualization_pub_   =   node_.advertise<visualization_msgs::Marker>(odom_track_visualization_topic_, 1, true);
                odom_track_pub_                 =   node_.advertise<racingslam::CarTrack>(odom_track_topic_, 1, true);
            }

            // coordinate transformation
            fsd_common_msgs::CarState transformCarState(const fsd_common_msgs::CarState &original, const Eigen::Matrix4d &transform);
            void lidar2car(pcl::PointCloud<pcl::PointXYZ>::Ptr &source);

        private:

            // tf
            tf2_ros::TransformBroadcaster br_car_odom_;
            geometry_msgs::TransformStamped transform_car_odom_;
            std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
            std::unique_ptr<tf2_ros::TransformListener> tf_listener_;
            double tf_listener_time_limit_ = 0;

            // private methods
            void getLidarTransform();

            void setTransformBasis(geometry_msgs::TransformStamped &transform, const std::string &parent_frame, const std::string &child_frame) {

                transform.header.stamp = ros::Time::now();
                transform.header.frame_id = parent_frame;
                transform.child_frame_id = child_frame;

            }

            void setTranslation(geometry_msgs::TransformStamped &transform, const double x, const double y, const double z) {

                transform.transform.translation.x = x;
                transform.transform.translation.y = y;
                transform.transform.translation.z = z;

            }

            void setRotation(geometry_msgs::TransformStamped &transform, tf2::Quaternion &q) {

                transform.transform.rotation.x = q.x();
                transform.transform.rotation.y = q.y();
                transform.transform.rotation.z = q.z();
                transform.transform.rotation.w = q.w();

            }

    };

}

