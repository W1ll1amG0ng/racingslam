#pragma once

#include "cone_in_slam.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/ConeDetections.h"

#include <ros/ros.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <vector>
#include <string>

namespace racingslam
{
    class Mapping
    {
    public:
        Mapping(std::string ns);
        void regTopic();
        void loadParam();
        fsd_common_msgs::ConeDetections getMap();
        fsd_common_msgs::Map getMapWithCloro();
        void coneFilter();
        void coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg);
        void stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg);
        void sendVisualization(std::string frame_id);
        void startSlam();

    private:

        //sub & pub
        ros::NodeHandle node_;
        ros::Subscriber cone_sub_;
        ros::Subscriber state_sub_;
        ros::Publisher map_pub_;
        ros::Publisher map_visualization_pub_;
        ros::Publisher realtime_map_pub_;


        //以下参数会随着程序的运行发生改变，为中间量
        std::vector<ConeInSlam> buffer_;
        fsd_common_msgs::CarState current_state_;
        fsd_common_msgs::CarState last_state_;
        fsd_common_msgs::CarState init_state_;
        double odom_;
        double delta_dis_;
        double loop_detection_dis_;
        int loop_detection_timer_;
        bool is_state_init_;
        bool is_loop_closed_;
        int cone_run_count_; //用于记录椎桶回调函数运行次数，每五次运行一次椎桶过滤器
        
        //以下参数是人为设置的参数，不会随着程序的运行而改变
        int min_update_count_;
        double max_reg_dis_;
        double min_cone_distance_;//
        int loop_detection_timer_count_;//
        std::string cone_sub_topic_;
        std::string state_sub_topic_;
        std::string map_pub_topic_;
        std::string map_visualization_pub_topic_;
        std::string realtime_map_pub_topic_;
        std::string frame_id_;


        fsd_common_msgs::ConeDetections translateConeState(const fsd_common_msgs::ConeDetections& original_cones);
        void regCones(fsd_common_msgs::ConeDetections& cone);
        bool loopDetect();
        double getDeltaDis();

        //私有方法
        double dis(const fsd_common_msgs::Cone a){return sqrt(a.position.x*a.position.x + a.position.y*a.position.y);}
        double dis(const fsd_common_msgs::Cone a, const fsd_common_msgs::Cone b)
        {
            return sqrt((a.position.x - b.position.x)*(a.position.x - b.position.x) + (a.position.y - b.position.y)*(a.position.y - b.position.y));
        }
        double dis(const ConeInSlam& a, const ConeInSlam& b)
        {
            geometry_msgs::Point a_pos = a.getPosition();
            geometry_msgs::Point b_pos = b.getPosition();

            return sqrt((a_pos.x-b_pos.x)*(a_pos.x-b_pos.x) + (a_pos.y-b_pos.y)*(a_pos.y-b_pos.y));
        }
        double Mapping::dis(const fsd_common_msgs::CarState a)
        {
            return sqrt(a.car_state.x*a.car_state.x + a.car_state.y*a.car_state.y);
        }
    };
}