#include "mapping.h"

using namespace racingslam;

Mapping::Mapping(std::string ns)
{
    node_ = ros::NodeHandle(ns);
    delta_dis_ = 0;
    odom_ = 0;
    loop_detection_timer_ = -1;
    cone_run_count_ = 0;
    is_state_init_ = false;
    is_loop_closed_ = false;
}

/***
 * @brief 该函数用于加载参数服务器的参数
*/
void Mapping::loadParam()
{
    if (!node_.param<std::string>("mapping/cone_sub_topic", cone_sub_topic_, "/perception/lidar/cone_side")) 
    {
        ROS_WARN_STREAM("Did not load mapping/cone_sub_topic. Standard value is: " << cone_sub_topic_);
    }
    if(!node_.param<std::string>("mapping/state_sub_topic", state_sub_topic_, "/estimation/slam/state"))
    {
        ROS_WARN_STREAM("Did not load mapping/state_sub_topic. Standard value is: " << state_sub_topic_);
    }
    if(!node_.param<std::string>("mapping/map_pub_topic", map_pub_topic_, "/estimation/slam/map"))
    {
        ROS_WARN_STREAM("Did not load mapping/map_pub_topic. Standard value is: " << map_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/map_visualization_pub_topic", map_visualization_pub_topic_, "/visualization/slam/map"))
    {
        ROS_WARN_STREAM("Did not load mapping/map_visualization_pub_topic. Standard value is: " << map_visualization_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/realtime_map_pub_topic", realtime_map_pub_topic_, "/estimation/slam/realtime_map"))
    {
        ROS_WARN_STREAM("Did not load mapping/realtime_map_pub_topic. Standard value is: " << realtime_map_pub_topic_);
    }
    if(!node_.param("mapping/min_update_count", min_update_count_, 3))
    {
        ROS_WARN_STREAM("Did not load mapping/min_update_count. Standard value is: " << min_update_count_);
    }
    if(!node_.param("mapping/max_reg_dis", max_reg_dis_, 1.5))
    {
        ROS_WARN_STREAM("Did not load mapping/max_reg_dis. Standard valus is: " << max_reg_dis_);
    }
    if(!node_.param("mapping/min_cone_distance", min_cone_distance_, 1.5))
    {
        ROS_WARN_STREAM("Did noe load mapping/min_cone_distance. Standard value is: " << min_cone_distance_);
    }
    if(!node_.param("mapping/loop_detection_timer_count", loop_detection_timer_count_, 10))
    {
        ROS_WARN_STREAM("Did not load mapping/loop_detection_time_count. Standard value is: " << loop_detection_timer_count_);
    }
    if(!node_.param<std::string>("mapping/visualization_frame_id", frame_id_, "world"))
    {
        ROS_WARN_STREAM("Did not load mapping/visualization_frame_id. Standard value is: " << frame_id_);
    }
}

/***
 * @brief 该函数用于接收与注册话题
*/
void Mapping::regTopic()
{
    cone_sub_ = node_.subscribe<fsd_common_msgs::ConeDetections>(cone_sub_topic_, 1, Mapping::coneCallback, this);
    state_sub_ = node_.subscribe<fsd_common_msgs::CarState>(state_sub_topic_, 1, Mapping::stateCallback, this);

    map_pub_ = node_.advertise<fsd_common_msgs::Map>(map_pub_topic_, 10, true);
    map_visualization_pub_ = node_.advertise<visualization_msgs::MarkerArray>(map_visualization_pub_topic_, 10, true);
    realtime_map_pub_ = node_.advertise<fsd_common_msgs::ConeDetections>(realtime_map_pub_topic_, 10, true);
}

/***
 * @brief 将椎桶从雷达坐标系转换到世界坐标系
*/
fsd_common_msgs::ConeDetections Mapping::translateConeState(const fsd_common_msgs::ConeDetections& original_cones)
{
    fsd_common_msgs::ConeDetections target_cones;

    for(auto i : original_cones.cone_detections)
    {
        fsd_common_msgs::Cone temp;
        temp.color = i.color;
        temp.colorConfidence = i.colorConfidence;
        temp.poseConfidence = i.poseConfidence;
        temp.position.x = current_state_.car_state.x + (i.position.x)*cos(current_state_.car_state.theta) - i.position.y*sin(current_state_.car_state.theta);
        temp.position.y = current_state_.car_state.y + i.position.y*cos(current_state_.car_state.theta) + (i.position.x)*sin(current_state_.car_state.theta);

        target_cones.cone_detections.push_back(i);
    }
}

/***
 * @brief 该函数用于注册椎桶    第一次将第一帧的所有椎桶数据输入buffer，之后将每帧得到的椎桶数据与之前已有的椎桶数据进行匹配，得到相邻最近的椎桶对，
 * 若该对椎桶同时满足阈值条件，则认为这个椎桶对为同一个椎桶，更新椎桶位置
*/
void Mapping::regCones(fsd_common_msgs::ConeDetections& cone)
{
    if(!buffer_.empty())
    {
        for(auto i: cone.cone_detections)
        {
            ConeInSlam temp(i);
            buffer_.push_back(temp);
        }
        return;
    }
    else
    {
        for(auto i: cone.cone_detections)
        {
            double min_dis = std::numeric_limits<double>::max();
            std::shared_ptr<ConeInSlam> cone_to_update;
            for(auto j: buffer_)
            {
                fsd_common_msgs::Cone temp = j.getCone();
                double dis = Mapping::dis(i,temp);
                if(dis < min_dis)
                {
                    min_dis = dis;
                    cone_to_update = std::make_shared<ConeInSlam>(j);
                }
            }
            if(min_dis <= max_reg_dis_)
            {
                cone_to_update -> update(i);
            }
            if(min_dis > max_reg_dis_)
            {
                ConeInSlam temp(i);
                buffer_.push_back(temp);
            }
        }
    }
}

/***
 * @brief 该函数用于将buffer中符合条件的椎桶提取出来生成地图，无颜色信息
*/
fsd_common_msgs::ConeDetections Mapping::getMap()
{
    fsd_common_msgs::ConeDetections map;
    for(auto i: buffer_)
    {
        if(i.getUpdateCount() >= min_update_count_)
        {
            map.cone_detections.push_back(i.getCone());
        }
    }
    return map;
}

/***
 * @brief 该函数用于将buffer中符合条件的椎桶提取出来生成地图，有颜色信息
*/
fsd_common_msgs::Map Mapping::getMapWithCloro()
{
    fsd_common_msgs::Map map;
    for(auto i: buffer_)
    {
        if(i.getUpdateCount() >= min_update_count_)
        {
            fsd_common_msgs::Cone temp;
            temp = i.getCone();
            if(temp.color.data == "r") map.cone_red.push_back(temp);
            if(temp.color.data == "b") map.cone_blue.push_back(temp);
        }
    }
    
    return map;
}

/**
 * @brief 对椎桶进行滤波，将两个距离过近的椎桶取平均值之后合并
 * 
 */
void Mapping::coneFilter() 
{

    for(auto i = buffer_.begin(); i != buffer_.end(); i++) {

        double min_dis = std::numeric_limits<double>::max();
        std::vector<ConeInSlam>::iterator cone_to_del;

        for(auto j = buffer_.begin(); j != buffer_.end(); j++) {

            if(i == j) continue;

            double current_dis = dis(*i, *j);
            if(current_dis < min_dis && current_dis <= min_cone_distance_ && i->getCone().color == j->getCone().color) {
                min_dis = current_dis;
                cone_to_del = j;
            }

        }

        if(min_dis == std::numeric_limits<double>::max()) {
            continue;
        }

        geometry_msgs::Point i_pos = i->getPosition();
        geometry_msgs::Point other_pos = cone_to_del->getPosition();
        i->setPosition((i_pos.x+other_pos.x)/2, (i_pos.y+other_pos.y)/2);
        buffer_.erase(cone_to_del);
    }
}

/***
 * @brief 椎桶的回调函数
*/
void Mapping::coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg)
{
    cone_run_count_++;
    fsd_common_msgs::ConeDetections cones;
    cones = Mapping::translateConeState(*msg);
    regCones(cones);

    if(cone_run_count_ / 5 == 1)
    {
        coneFilter();
    }

}

/***
 * @brief 该函数用于进行回环检测，若检测到已经完成回环，则发送真值
*/
bool Mapping::loopDetect()
{
    double current_state_dis = dis(current_state_);
    double last_state_dis = dis(last_state_);
    delta_dis_ = (delta_dis_ + (current_state_dis - last_state_dis)) / 2;

    ROS_INFO("current dis: %f; last dis: %f, delta: %f", current_state_dis, last_state_dis, delta_dis_);

    if(current_state_dis <= loop_detection_dis_ && odom_ >= 10*loop_detection_dis_) 
    {

        if(delta_dis_ > 0 && loop_detection_timer_ < 0)
        {
            ROS_INFO("TIMER ACTIVE! ");
            loop_detection_timer_ = 0;
        }
        else if(delta_dis_ > 0 && loop_detection_timer_ >= 0)
        {
            loop_detection_timer_++;
        }
        else if(delta_dis_ < 0 && loop_detection_timer_ >= 0)
        {
            ROS_INFO("TIMER_INACTIVE! ");
            loop_detection_timer_ = -1;
        }

        if(loop_detection_timer_ >= loop_detection_timer_count_)
        {
            is_loop_closed_ = true;
        }

    }
    else 
    {

        if(loop_detection_timer_ >= 0)
        {
            ROS_INFO("TIMER_INACTIVE! ");
            loop_detection_timer_ = -1;
        }
    }
}

/***
 * @brief 该函数是车辆位置的回调函数，用于处理接受到的车辆的位姿信息
*/
void Mapping::stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg)
{
    if(!is_state_init_)
    {
        init_state_.car_state.x     = msg -> car_state.x + 2.4*cos(msg->car_state.theta);
        init_state_.car_state.y     = msg -> car_state.y + 2.4*sin(msg->car_state.theta);
        init_state_.car_state.theta = 0 * msg -> car_state.theta;
        init_state_.header = msg -> header;

        is_state_init_ = true;
    }

    last_state_ = current_state_;

    current_state_.car_state.x     = msg -> car_state.x + 2.4*cos(msg->car_state.theta);
    current_state_.car_state.y     = msg -> car_state.y + 2.4*sin(msg->car_state.theta);
    current_state_.car_state.theta = 0 * msg -> car_state.theta;
    current_state_.header = msg -> header;

    ROS_INFO("current car state x = %f, y = %f, theta = %f", current_state_.car_state.x, current_state_.car_state.y, current_state_.car_state.theta);

    odom_ += getDeltaDis();

    Mapping::loopDetect();
}

/***
 * @brief 该函数用来发送可视化话题信息
*/
void Mapping::sendVisualization(std::string frame_id)
{
    visualization_msgs::Marker cone_marker;
    visualization_msgs::MarkerArray cone_maker_array;

    //Set the marker type
    cone_marker.type = visualization_msgs::Marker::CUBE;
    cone_marker.action = visualization_msgs::Marker::ADD;

    cone_marker.header.stamp = ros::Time::now();
    cone_marker.header.frame_id = frame_id;
														   
    cone_marker.ns = "slam";

    cone_marker.lifetime = ros::Duration();
    cone_marker.frame_locked = true;

    //Set the color
    cone_marker.color.r = 1.0f;
    cone_marker.color.g = 0.0f;
    cone_marker.color.b = 0.0f;
    cone_marker.color.a = 0.8f;

    cone_marker.scale.x = 0.2;
    cone_marker.scale.y = 0.2;
    cone_marker.scale.z = 0.3;

    // default val to fix warn of rviz
    cone_marker.pose.orientation.x = 0;
    cone_marker.pose.orientation.y = 0;
    cone_marker.pose.orientation.z = 0;
    cone_marker.pose.orientation.w = 1;

    fsd_common_msgs::ConeDetections map;
    map = Mapping::getMap();

    for(auto i: map.cone_detections)
    {
        cone_marker.id++;
        cone_marker.pose.position.x = i.position.x;
        cone_marker.pose.position.y = i.position.y;

        if(i.color.data == "r")
        {
            cone_marker.color.r = 1.0f;
            cone_marker.color.g = 0.0f;
            cone_marker.color.b = 0.0f;
        }
        else if(i.color.data == "b")
        {
            cone_marker.color.r = 0.0f;
            cone_marker.color.g = 0.0f;
            cone_marker.color.b = 1.0f;
        }
        else
        {
            cone_marker.color.r = 0.0f;
            cone_marker.color.g = 1.0f;
            cone_marker.color.b = 0.0f;
        }

        cone_maker_array.markers.push_back(cone_marker);
    }

    map_visualization_pub_.publish(cone_maker_array);
}

/***
 * @brief 该函数用于得到车辆两位置之间的距离
*/
double Mapping::getDeltaDis()
{
    double delta_x = current_state_.car_state.x - last_state_.car_state.x;
    double delta_y = current_state_.car_state.y - last_state_.car_state.y;

    double res = sqrt(delta_x*delta_x + delta_y*delta_y);

    return res;
}

/***
 * @brief 该函数用于开始进行slam
*/
void Mapping::startSlam()
{
    fsd_common_msgs::Map map;
    if(is_loop_closed_)
    {
        map = Mapping::getMapWithCloro();
        map_pub_.publish(map);
    }

    fsd_common_msgs::ConeDetections realtime_map;
    realtime_map = Mapping::getMap();
    realtime_map_pub_.publish(realtime_map);

    Mapping::sendVisualization(frame_id_);
}