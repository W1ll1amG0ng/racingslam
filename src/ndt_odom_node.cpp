#include "ndt_odom.h"

using namespace racingslam;

int main(int argc, char **argv) {
    ros::init(argc, argv, "ndt_odom_node");
    NDTOdometry handle = NDTOdometry("");
    handle.init();
    handle.regTopics();

    while(ros::ok()) {
        ros::spinOnce();
    }
    return 0;
}