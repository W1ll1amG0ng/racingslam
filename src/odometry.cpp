#include "odometry.h"

#include <tf2_eigen/tf2_eigen.h>
#include <pcl/common/transforms.h>

using namespace racingslam;

Odometry::Odometry(std::string ns) : Handle(ns) {

    tf_buffer_ = std::make_unique<tf2_ros::Buffer>();
    tf_listener_ = std::make_unique<tf2_ros::TransformListener>(*tf_buffer_);

    zero_pos_.car_state.x = 0;
    zero_pos_.car_state.y = 0;
    zero_pos_.car_state.theta = 0;

}

void Odometry::init() {

    loadParam();

    if(odom_frame_name_ == "") {
        ROS_ERROR("Error: odom frame name is undefinded!");
        exit(1);
    } else if(world_frame_name_ == "") {
        ROS_ERROR("Error: world frame name is undefinded!");
        exit(1);
    } else if(car_frame_name_ == "") {
        ROS_ERROR("Error: car frame name is undefinded!");
        exit(1);
    } else if(lidar_frame_name_ == "") {
        ROS_ERROR("Error: lidar frame name is undefinded!");
        exit(1);
    } else if(tf_listener_time_limit_ == 0) {
        ROS_ERROR("Error: TF listener time limit is not inited! ");
        exit(1);
    } else if(odom_track_topic_ == "") {
        ROS_ERROR("Error: odom track topic is not inited! ");
        exit(1);
    } else if(odom_track_visualization_topic_ == "") {
        ROS_ERROR("Error: odom track visualization topic is not inited! ");
        exit(1);
    }

    getLidarTransform();

    odom_track_.header.stamp = ros::Time::now();
    odom_track_.header.frame_id = odom_frame_name_;

    is_inited_ = true;

}

void Odometry::sendTransform() {

    if(!is_inited_) {
        ROS_ERROR("Error: odometry has not been inited !!!");
        return;
    }
    if(odom_track_.track.empty()) {
        ROS_WARN("Error: track has not been inited !");
        return;
    }

    fsd_common_msgs::CarState current_pos = odom_track_.track.back();

    setTransformBasis(transform_car_odom_, odom_frame_name_, car_frame_name_);
    setTranslation(transform_car_odom_, current_pos.car_state.x, current_pos.car_state.y, 0.0);
    tf2::Quaternion q;
    q.setRPY(0, 0, current_pos.car_state.theta);
    setRotation(transform_car_odom_, q);

    br_car_odom_.sendTransform(transform_car_odom_);
    ROS_INFO("%s --> car TF sended. ", odom_frame_name_.c_str());

}

void Odometry::getLidarTransform() {

    bool is_succeed = false;
    while(!is_succeed) {

        try {
            transform_lidar2car_ = tf_buffer_->lookupTransform(car_frame_name_, lidar_frame_name_, ros::Time(0), ros::Duration(tf_listener_time_limit_));
            ROS_INFO("TF data init completed! ");
            is_succeed = true;
        } catch(tf2::TransformException &ex) {
            ROS_WARN("Warning: %s", ex.what());
        }

    }
}

void Odometry::loadBasicParam() {

    if (!node_.param<std::string>("odometry/world_frame_name", world_frame_name_, "world")) {
        ROS_WARN_STREAM("Did not load odometry/world_frame_name. Standard value is: " << world_frame_name_);
    }
    if (!node_.param<std::string>("odometry/car_frame_name", car_frame_name_, "car")) {
        ROS_WARN_STREAM("Did not load odometry/world_frame_name. Standard value is: " << car_frame_name_);
    }
    if (!node_.param<std::string>("odometry/lidar_frame_name", lidar_frame_name_, "rslidar")) {
        ROS_WARN_STREAM("Did not load odometry/lidar_frame_name. Standard value is: " << lidar_frame_name_);
    }
    if (!node_.param<std::string>("odometry/odom_track_visualization_topic", odom_track_visualization_topic_, "/racingslam/odom_track_visualization")) {
        ROS_WARN_STREAM("Did not load odometry/odom_track_visualization_topic. Standard value is: " << odom_track_visualization_topic_);
    }
    if (!node_.param<std::string>("odometry/odom_track_topic", odom_track_topic_, "/racingslam/odom_track")) {
        ROS_WARN_STREAM("Did not load odometry/odom_track_topic. Standard value is: " << odom_track_topic_);
    }
    if (!node_.param<double>("odometry/tf_listener_time_limit", tf_listener_time_limit_, 3.0)) {
        ROS_WARN_STREAM("Did not load odometry/tf_listener_time_limit. Standard value is: " << tf_listener_time_limit_);
    }

}



fsd_common_msgs::CarState Odometry::transformCarState(const fsd_common_msgs::CarState &original, const Eigen::Matrix4d &transform) {

    // init res
    fsd_common_msgs::CarState res;
    res.header = original.header;
    res.header.stamp = ros::Time::now();

    //init transform
    Eigen::Matrix3d rotation_matrix = transform.block<3, 3>(0, 0);
    Eigen::Vector3d translation_vector = transform.block<3, 1>(0, 3);

    Eigen::AngleAxisd yaw_angle(original.car_state.theta, Eigen::Vector3d::UnitZ());
    Eigen::Matrix3d original_rotation = yaw_angle.matrix();

    //right multiplication
    Eigen::Matrix3d final_rotation = original_rotation * rotation_matrix;
    Eigen::Vector3d final_angle = final_rotation.eulerAngles(0, 1, 2);

    res.car_state.x = original.car_state.x + translation_vector[0];
    res.car_state.y = original.car_state.y + translation_vector[1];
    res.car_state.theta = final_angle[2];

    double delta_theta = -atan2(rotation_matrix.coeff(1, 0), rotation_matrix.coeff(0, 0)); //输出日志信息
    ROS_INFO("rotation = %f, angle = %f", delta_theta, res.car_state.theta);
    ROS_INFO("x = %f, y = %f", res.car_state.x, res.car_state.y);

    return res;

}



void Odometry::lidar2car(pcl::PointCloud<pcl::PointXYZ>::Ptr &source) {

    pcl::PointCloud<pcl::PointXYZ>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Isometry3d matrix = tf2::transformToEigen(transform_lidar2car_);
    auto raw_matrix = matrix.matrix();
    pcl::transformPointCloud<pcl::PointXYZ>(*source, *temp_cloud, raw_matrix);

    source = temp_cloud;

}

void Odometry::sendCarTrackVisualization()
{
    visualization_msgs::Marker odom_track_line;
    odom_track_line.header.frame_id = odom_track_.header.frame_id;
    odom_track_line.header.stamp = ros::Time::now();
    odom_track_line.ns = node_.getNamespace() + "/odom_track_visualization";
    odom_track_line.type = visualization_msgs::Marker::LINE_STRIP;
    odom_track_line.action = visualization_msgs::Marker::ADD;

    odom_track_line.scale.x = 0.1; //线宽
    odom_track_line.color.r = 1.0; //红色
    odom_track_line.color.a = 1.0; //完全不透明

    for(auto i : odom_track_.track)
    {
        geometry_msgs::Point temp_point;
        temp_point.x = i.car_state.x;
        temp_point.y = i.car_state.y;
        odom_track_line.points.push_back(temp_point);
    }

    odom_track_visualization_pub_.publish(odom_track_line);

    return;
}