#include "cone_in_slam.h"

using namespace racingslam;

ConeInSlam::ConeInSlam(fsd_common_msgs::Cone cone)
{
    cone_ = cone;
    update_count_ = 0;
}


void ConeInSlam::update(fsd_common_msgs::Cone cone) 
{
    if(update_count_ == 0)
    {
        cone_.position           =    cone.position;
        cone_.color              =    cone.color;
        cone_.poseConfidence     =    cone.poseConfidence;
        cone_.colorConfidence    =    cone.colorConfidence;
    }
    else
    {
        cone_.position.x         =     (cone_.position.x + cone.position.x) / 2;
        cone_.position.y         =     (cone_.position.y + cone.position.y) / 2;
        cone_.poseConfidence     =     cone.poseConfidence;
        cone_.colorConfidence    =     cone.colorConfidence;
    }

    update_count_++;
}