#include "icp_odom.h"

using namespace racingslam;

int main(int argc,char** argv)
{
    ros::init(argc,argv,"icp_odom_node");

    ICPOdometry handle = ICPOdometry("");

    handle.init();
    handle.regTopics();

    while(ros::ok()) {
        ros::spinOnce();
    }
    
    return 0;
}