#include "icp_odom.h"

using namespace racingslam;

ICPOdometry::ICPOdometry(std::string ns) : Odometry(ns)
{
    current_min_dis_ = std::numeric_limits<double>::max();
    last_min_dis_ = std::numeric_limits<double>::max();
    is_last_cones_init_ = false;
    is_transform_mat_init_ = false;
    current_cones_pcl_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
    last_cones_pcl_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
}


void ICPOdometry::loadParam()
{
    loadBasicParam();

    if (!node_.param<std::string>("odometry/icp/frame_name", odom_frame_name_, "odom_icp")) 
    {
        ROS_WARN_STREAM("Did not load odometry/icp/frame_name. Standard value is: " << odom_frame_name_);
    }
    if (!node_.param<std::string>("odometry/icp/topic", cone_topic_, "/perception/lidar/cone_side")) 
    {
        ROS_WARN_STREAM("Did not load odometry/icp/topic. Standard value is: " << cone_topic_);
    }
    if (!node_.param("odometry/icp/min_dis2update", min_dis2update_, 0.5)) 
    {
        ROS_WARN_STREAM("Did not load odometry/icp/min_dis2update. Standard value is: " << min_dis2update_);
    }
    if (!node_.param("odometry/icp/max_dis2pass", max_dis2pass_, 4.0)) 
    {
        ROS_WARN_STREAM("Did not load odometry/icp/max_dis2pass. Standard value is: " << max_dis2pass_);
    }
    if (!node_.param("odometry/icp/max_iterations", max_iterations_, 100.0))
    {
        ROS_WARN_STREAM("Did not load odometry/icp/max_iterations. Standard value is: " << max_iterations_);
    }
    if (!node_.param("odometry/icp/convergence_condition", convergence_condition_, 1e-8))
    {
        ROS_WARN_STREAM("Did not load odometry/icp/convergence_condition. Standard value is: " << convergence_condition_);
    }
    if (!node_.param("odometry/icp/min_pointcloud_num", min_pointcloud_num_, 10.0))
    {
        ROS_WARN_STREAM("Did not load odometry/icp/min_pointcloud_num. Standard value is: " << min_pointcloud_num_);
    }

    return;
}


void ICPOdometry::regTopics()
{
    regBasicTopics();
    cones_visualization_pub_ = node_.advertise<visualization_msgs::MarkerArray>("/odom/cones/visaulization", 10, true);
    odom_track_visualization_pub_ = node_.advertise<visualization_msgs::Marker>("/odom/odom_track/visaulization", 10, true);
    cone_sub_ =  node_.subscribe<fsd_common_msgs::ConeDetections>(cone_topic_, 10, &ICPOdometry::coneCallback, this);

    return;
}


void ICPOdometry::coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg)
{
    //深拷贝
    current_cones_.header = msg->header;
    current_cones_.cone_detections = msg->cone_detections;
    //ROS_INFO("Cones updated.");
    
    ICPOdometry::calculateCarState();

    //strat visualization
    sendVisualization(odom_frame_name_);
    sendCarTrackVisualization();

    return;
}


//////////////////////////////////////////////////////////////////////////////////////////
//                          该段代码有莫名bug，固将其注释                                   //
//                                   bug内容                                            //
//                 在进行一次改变认定为相匹配的椎桶之后，无法正确计算最短距离                    //
//                     之后得出的最短距离不再发生变化，无法算出正确结果                        //
//                          鼠鼠我仍在努力寻找bug原因，TT                                  //
/////////////////////////////////////////////////////////////////////////////////////////

// void ICPOdometry::calculateCarState()
// {   
//     for(auto i : current_cones_.cone_detections) //此循环迭代出当前信息中最近的椎桶的信息
//     {
//         double current_dis = dis(i);
        
//         if(current_dis < max_dis2pass_)
//         {
//             continue;
//         }
        
//         if(current_min_dis_ > current_dis)
//         {
//             current_min_dis_ = current_dis;
//             current_min_dis_cone_ = i;
//         }
//     }
//     ROS_INFO("min_dis_ = %f",current_min_dis_);

//     if(last_min_dis_ == std::numeric_limits<double>::max())
//     {
//         last_min_dis_ = current_min_dis_;
//         last_min_dis_cone_ = current_min_dis_cone_;
//         return;
//     }

//     if(dis(last_min_dis_cone_,current_min_dis_cone_) <= min_dis2update_) //若上次椎桶与当前椎桶之间变换的距离小于阈值，则认为两椎桶相匹配，计算车辆位移
//     {
//         odom_delta_x_ = last_min_dis_cone_.position.x - current_min_dis_cone_.position.x;
//         odom_delta_y_ = last_min_dis_cone_.position.y - current_min_dis_cone_.position.y;
//         last_min_dis_ = current_min_dis_;
//         last_min_dis_cone_ = current_min_dis_cone_;
//         ROS_INFO("car_delta_x_ = %f, car_delta_y_ = %f",odom_delta_x_,odom_delta_y_);
//         ROS_INFO("=================================================================================");
//         return;
//     }
//     if(dis(last_min_dis_cone_,current_min_dis_cone_) > min_dis2update_) //若上次椎桶与当前椎桶之间变换的距离大于阈值，则认为两椎桶不匹配
//     {
//         last_min_dis_ = current_min_dis_;
//         last_min_dis_cone_ = current_min_dis_cone_;
//         return;
//     }
// }

//////////////////////////////////////////////////////////////////////////////////////////
//                 最小二乘算法，计算效果不理想，误差较大，有待优化算法                          //
//////////////////////////////////////////////////////////////////////////////////////////

// void ICPOdometry::calculateCarState()
// {
//     //计算车辆平移
//     if(!is_last_cones_init_)
//     {
//         last_cones_ = current_cones_;
//         is_last_cones_init_ = 1;
//         return;
//     }
    
//     double cone_delta_x;
//     double cone_delta_y;
//     std::vector<double> cone_delta_x_array;
//     std::vector<double> cone_delta_y_array;
//     double total_cone_delta_x;
//     double total_cone_delta_y;
//     double average_cone_delta_x;
//     double average_cone_delta_y;
    
//     for(auto i : current_cones_.cone_detections)
//     {
//         double min_dis = std::numeric_limits<double>::max();
//         for(auto j : last_cones_.cone_detections)
//         {
//             if(dis(i,j) < min_dis)
//             {
//                 min_dis = dis(i,j);
//             }

//             if(min_dis < min_dis2update_)
//             {
//                 cone_delta_x = j.position.x - i.position.x;
//                 cone_delta_y = j.position.y - i.position.y;
//                 cone_delta_x_array.push_back(cone_delta_x);
//                 cone_delta_y_array.push_back(cone_delta_y);
//             }
//         }
//     }

//     for(auto i : cone_delta_x_array)
//     {
//         total_cone_delta_x += i;
//     }
//     for(auto i : cone_delta_y_array)
//     {
//         total_cone_delta_y += i;
//     }

//     average_cone_delta_x = total_cone_delta_x/cone_delta_x_array.size();
//     average_cone_delta_y = total_cone_delta_y/cone_delta_y_array.size();

//     ROS_INFO("average cone delta x = %f",average_cone_delta_x);
//     ROS_INFO("average cone delta y = %f",average_cone_delta_y);
//     ROS_INFO("==============================================================================================");

//     last_cones_ = current_cones_;
//     cone_delta_x_array.clear();
//     cone_delta_y_array.clear();

//     //计算车辆转角
    
//     return;
// }


//使用pcl库进行匹配
void ICPOdometry::calculateCarState()
{
    if(!is_last_cones_init_)
    {
        last_cones_ = current_cones_;
        is_last_cones_init_ = 1;
        odom_track_.track.push_back(zero_pos_);
        sendTransform();
        current_cones_.cone_detections.clear();
        return;
    }

    pcl::PointCloud<pcl::PointXYZ> Final;

    //将椎桶看作点云将点云变量初始化
    for(auto i : last_cones_.cone_detections)
    {
        pcl::PointXYZ point;
        point.x = i.position.x;    point.y = i.position.y;    point.z = 0;
        last_cones_pcl_ -> points.push_back(point);
    }
    for(auto i : current_cones_.cone_detections)
    {
        pcl::PointXYZ point;
        point.x = i.position.x;    point.y = i.position.y;    point.z = 0;
        current_cones_pcl_ -> points.push_back(point);
    }

    //将雷达坐标转换为椎桶坐标
    lidar2car(current_cones_pcl_);
    lidar2car(last_cones_pcl_);

    pcl::IterativeClosestPoint<pcl::PointXYZ,pcl::PointXYZ> icp;

    icp.setInputSource(last_cones_pcl_);
    icp.setInputTarget(current_cones_pcl_);

    //第一次时将初始旋转矩阵初始化为单位矩阵
    if(!is_transform_mat_init_)
    {
        original_transformation_ = Eigen::Matrix4f::Identity();
    }

    //icp.setMaxCorrespondenceDistance(min_dis2update_); // 设置最大对应距离
    icp.setRANSACOutlierRejectionThreshold(int(min_pointcloud_num_)); //设置最少点云匹配数目
    icp.setMaximumIterations(int(max_iterations_)); // 设置最大迭代次数
    icp.setTransformationEpsilon(convergence_condition_); // 设置收敛条件

    icp.align(Final,original_transformation_);

    transformation_ = icp.getFinalTransformation();

    original_transformation_ = transformation_; //将上一帧的变换矩阵当作下一帧的初始矩阵

    //更新车辆位姿
    fsd_common_msgs::CarState last_state = odom_track_.track.back();
    fsd_common_msgs::CarState current_state = transformCarState(last_state, transformation_.cast<double>());

    //将新位置添加进里程计轨迹
    odom_track_.track.push_back(current_state);

    sendTransform();

    // //旋转顺序RxRyRz，这表示先绕z 再绕y 最后绕x
    // Eigen::Matrix3d rotation_matrix = transformation_.block<3, 3>(0, 0);
    // Eigen::Vector3d translation_vector = transformation_.block<3, 1>(0, 3);

    // //计算欧拉角以及平移向量
    // odom_delta_x_ = -translation_vector.x();
    // odom_delta_y_ = -translation_vector.y();
    // odom_delta_theta_ = -atan2(rotation_matrix.coeff(1, 0), rotation_matrix.coeff(0, 0));

    // // 将矩阵和向量转换为字符串
    // std::ostringstream stream_rotation;
    // stream_rotation << rotation_matrix;
    // std::ostringstream stream_translation;
    // stream_translation << translation_vector;

    // // 打印信息
    // ROS_INFO("\n"
    //         "========== rotation matrix ==========\n"
    //         "%s\n"
    //         "========== translation vector ==========\n"
    //         "%s\n"
    //         "========== euler angles ==========\n"
    //         "%f\n"
    //         "========== translation x and y ==========\n"
    //         "x = %f    y = %f\n"
    //         "========================================================================================",
    //         stream_rotation.str().c_str(), stream_translation.str().c_str(), odom_delta_theta_, odom_delta_x_, odom_delta_y_);

    last_cones_ = current_cones_;

    last_cones_pcl_.reset(new pcl::PointCloud<pcl::PointXYZ>);
    current_cones_pcl_.reset(new pcl::PointCloud<pcl::PointXYZ>);
}


void ICPOdometry::sendVisualization(std::string frame_id)
{
    visualization_msgs::Marker cone_marker;
    visualization_msgs::MarkerArray cone_marker_array;

    //set the marker type
    cone_marker.type = visualization_msgs::Marker::CUBE;
    cone_marker.action = visualization_msgs::Marker::ADD;

    cone_marker.header.stamp = ros::Time::now();
    cone_marker.header.frame_id = frame_id;

    cone_marker.ns = "rslidar_cones";

    cone_marker.lifetime = ros::Duration();
    cone_marker.frame_locked = true;

    //Set the color
    cone_marker.color.r = 1.0f;
    cone_marker.color.g = 0.0f;
    cone_marker.color.b = 0.0f;
    cone_marker.color.a = 0.8f;

    cone_marker.scale.x = 0.2;
    cone_marker.scale.y = 0.2;
    cone_marker.scale.z = 0.3;

    // default val to fix warn of rviz
    cone_marker.pose.orientation.x = 0;
    cone_marker.pose.orientation.y = 0;
    cone_marker.pose.orientation.z = 0;
    cone_marker.pose.orientation.w = 1;

    for(auto i : current_cones_.cone_detections)
    {
        cone_marker.id ++;
        cone_marker.pose.position.x = i.position.x;
        cone_marker.pose.position.y = i.position.y;
        
        if(i.color.data == "r")
        {
            cone_marker.color.r = 1.0f;
            cone_marker.color.g = 0.0f;
            cone_marker.color.b = 0.0f;
        }
        else if(i.color.data == "b")
        {
            cone_marker.color.r = 0.0f;
            cone_marker.color.g = 0.0f;
            cone_marker.color.b = 1.0f;
        }
        else
        {
            cone_marker.color.r = 0.0f;
            cone_marker.color.g = 1.0f;
            cone_marker.color.b = 0.0f;
        }

        cone_marker_array.markers.push_back(cone_marker);
    }

    cones_visualization_pub_.publish(cone_marker_array);

    return;
}